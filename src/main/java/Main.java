public class Main {

    public static void main(String[] args) {
        Cinema cinema = new Cinema("Prestige", 9, 20);
        cinema.getSeats();
        if(cinema.reserveSeat("B11")) {
            System.out.println("Please complete the payment");
        } else {
            System.out.println("Sorry, this seat is taken");
        }
//        if(cinema.reserveSeat("B11")) {
//            System.out.println("Please complete the payment");
//        } else {
//            System.out.println("Sorry, this seat is taken");
//        }
    }
}